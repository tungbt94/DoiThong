package ultils;

import java.util.Random;

/**
 * Created by tungbt94 on 16/08/2016.
 */
public class CommonValues {
    public static final String NAME = "Doi Thong";

    public static final int SCREEN_WIDTH = 1260;
    public static final int SCREEN_HEIGHT = 721;

    public static boolean GAME_RUNNING = true;

    public static final int PINE_DISTANCE = 150;

    public static final Random RANDOM = new Random();
}
