package controllers.pine;

import controllers.PlayerController;
import controllers.SingleController;
import models.GameObject;
import models.Pine;
import ultils.CommonValues;
import views.GameDrawer;
import views.ImageDrawer;

/**
 * Created by DUC on 8/13/2016.
 */
public class PineController extends SingleController {
    private static final int SPEED = 3;
    private static final int MAX_HIGHT = 300;
    private static final int MIN_HIGHT = 680;
    private int distance;
    private boolean changed;


    public PineController(GameObject gameObject, GameDrawer gameDrawer) {
        super(gameObject, gameDrawer);
        this.gameVector.dx = -SPEED;
        changed = false;
    }

    public static PineController create(int x, int y, PineType type) {
        PineController pineController = null;
        switch (type) {
            case GREEN: {
                pineController = new PineController(
                        new Pine(x, y),
                        new ImageDrawer("resources/3.png"));
                break;
            }
        }
        return pineController;
    }

    public void prick(int dy){
        if( this.getGameObject().getY() + dy <= MAX_HIGHT ) {
            this.gameVector.dy = dy;
            changed = false;
        } else if (this.getGameObject().getY() + dy > MAX_HIGHT && this.getGameObject().getY() + dy < MIN_HIGHT)
            this.gameVector.dy = -dy;
    }

    public void run(){
        super.run();
        if(CommonValues.RANDOM.nextInt(300) % 10 == 0 && !changed) {
            distance = PlayerController.instance.getGameObject().getBottom() - this.gameObject.getY();
            changed = true;
        }
        if (distance > 0){
            prick(distance / 10);
        }
    }
}
