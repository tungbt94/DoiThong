package controllers.pine;

import controllers.ControllerManager;
import ultils.CommonValues;

import java.util.Random;

/**
 * Created by DUC on 8/13/2016.
 * Updated by Duong on 8/16/2016.
 * Updated by tungbt94 on 8/16/2016.
 */
public class PineControllerManager extends ControllerManager {
    private int count;
    private int respawn;
//    private int time_to_prick;


    private PineControllerManager() {
        super();
        respawn = 100;
        count = respawn;
//        time_to_prick = rand.nextInt(300 - 100) + 100;
    }

    public void setRespawn(int respawn) {
        this.respawn = respawn;
    }

    public void run() {
        super.run();
        count--;
        int enX = CommonValues.SCREEN_WIDTH + CommonValues.PINE_DISTANCE;
        int enY = CommonValues.RANDOM.nextInt(600 - 250) + 250;
        if (count == 0) {
            count = respawn;
            PineController pineController = PineController.create(
                    enX, enY, PineType.GREEN);
            this.add(pineController);
        }
//        time_to_prick --;
//        if(time_to_prick == 0 && singleControllerVector.size() > 0) {
//            PineController pineController = (PineController) singleControllerVector.get(rand.nextInt(singleControllerVector.size() - 1 - 1) );
//            pineController.prick();
//            time_to_prick = rand.nextInt(300 - 100) + 100;
//        }
    }

    public static final PineControllerManager instance = new PineControllerManager();
}
